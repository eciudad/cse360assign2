/*Student Name: Erick Ciudad
 * Class ID: 350
 * Description: Class that contains multiple methods
 * for arithmetic operations
 * */


package cse360assign2;


//class for Calculator, containing a total property
//and arithmetic methods
public class Calculator {

	private int total;
	private String history;
	
	public Calculator () {
		total = 0;  // not needed - included for clarity
		history = "0 ";
	}
	
	/**
	 * getter method of total
	 * @return
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 * adds onto the calculators value
	 * @param value		number to be added to total
	 */
	public void add (int value) {
		total += value;
		
		history += "+ " + value + " ";
		
	}
	
	/**
	 * subtracts from the calculators value
	 * @param value		number to be subtracted from total
	 */
	public void subtract (int value) {
		total -= value;
		history += "- " + value + " ";

	}
	
	/**
	 * multiplies the calculators value
	 * @param value		number to multiply the total
	 */
	public void multiply (int value) {
		total = total * value;
		
		history += "* " + value + " ";

	}
	
	/**
	 * divides the calculators value
	 * @param value		number to divide the total, doesn't accept zero
	 */
	public void divide (int value) {
		if (value != 0) {
			total = total / value;
			history += "/ " + value + " ";

		}
		else {
			total = 0;
		}
	}
	
	/**
	 * returns history of calculations, method calls in Calculator
	 */
	public String getHistory () {
		return history;
	}
}